CREATE TABLE Proveedor (
    ProveedorID INT PRIMARY KEY,
    ProveedorNombre VARCHAR(40),
	ProveedorTelefono VARCHAR(25),
    ProveedorDireccion VARCHAR(50)
);
CREATE TABLE Equipo (
    EquipoID INT PRIMARY KEY,
    ProveedorID INT,
    Especificaciones VARCHAR(40),
    FechaServicio date,
    MantenimientoPrevio VARCHAR(40),
    FOREIGN KEY (ProveedorID) REFERENCES Proveedor(ProveedorID)
);
CREATE TABLE Cliente (
    ClienteID INT PRIMARY KEY,
    ClienteNombre VARCHAR(20),
	ClienteApellidos VARCHAR(40),
    ClienteTelefono VARCHAR(20),
	ClienteDirrecion VARCHAR(50)
);

CREATE TABLE InformeMantenimiento (
    InformeID INT PRIMARY KEY,
    OrdenID INT,
    Fecha date,
    TipoDeMantenimiento VARCHAR(65),
    TiempoEmpleado VARCHAR(15),
    MaterialesUtilizados VARCHAR(65),
    FOREIGN KEY (OrdenID) REFERENCES OrdenDeTrabajo(OrdenID)
);
CREATE TABLE OrdenDeTrabajo (
    OrdenID INT PRIMARY KEY,
    ClienteID INT,
    EquipoID INT,
    OrdenFecha date,
    OrdenPrioridad VARCHAR(20),
    DescripcionTrabajo VARCHAR(40),
    FOREIGN KEY (ClienteID) REFERENCES Cliente(ClienteID),
    FOREIGN KEY (EquipoID) REFERENCES Equipo(EquipoID)
);
CREATE TABLE TareaMantenimiento (
    TareaID INT PRIMARY KEY,
    OrdenID INT,
    FechaLimite date,
    Prioridad VARCHAR(65),
    FOREIGN KEY (OrdenID) REFERENCES OrdenDeTrabajo(OrdenID)
);
CREATE TABLE Reportes (
    ReportesID SERIAL PRIMARY KEY,
    NombreTabla VARCHAR(50),
    TipoOperacion VARCHAR(10),
    OperacionTiempo TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    OperacionDetalles TEXT
);
CREATE OR REPLACE FUNCTION log_proveedor_changes() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('Proveedor', 'INSERT', 'Inserted ID: ' || NEW.ProveedorID || ', Name: ' || NEW.ProveedorNombre);
    ELSIF TG_OP = 'UPDATE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('Proveedor', 'UPDATE', 'Updated ID: ' || NEW.ProveedorID || ', Name: ' || NEW.ProveedorNombre);
    ELSIF TG_OP = 'DELETE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('Proveedor', 'DELETE', 'Deleted ID: ' || OLD.ProveedorID || ', Name: ' || OLD.ProveedorNombre);
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_proveedor_changes
AFTER INSERT OR UPDATE OR DELETE ON Proveedor
FOR EACH ROW EXECUTE FUNCTION log_proveedor_changes();

CREATE OR REPLACE FUNCTION log_equipo_changes() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('Equipo', 'INSERT', 'Inserted ID: ' || NEW.EquipoID || ', Specifications: ' || NEW.Especificaciones);
    ELSIF TG_OP = 'UPDATE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('Equipo', 'UPDATE', 'Updated ID: ' || NEW.EquipoID || ', Specifications: ' || NEW.Especificaciones);
    ELSIF TG_OP = 'DELETE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('Equipo', 'DELETE', 'Deleted ID: ' || OLD.EquipoID || ', Specifications: ' || OLD.Especificaciones);
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_equipo_changes
AFTER INSERT OR UPDATE OR DELETE ON Equipo
FOR EACH ROW EXECUTE FUNCTION log_equipo_changes();

CREATE OR REPLACE FUNCTION log_cliente_changes() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('Cliente', 'INSERT', 'Inserted ID: ' || NEW.ClienteID || ', Name: ' || NEW.ClienteNombre);
    ELSIF TG_OP = 'UPDATE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('Cliente', 'UPDATE', 'Updated ID: ' || NEW.ClienteID || ', Name: ' || NEW.ClienteNombre);
    ELSIF TG_OP = 'DELETE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('Cliente', 'DELETE', 'Deleted ID: ' || OLD.ClienteID || ', Name: ' || OLD.ClienteNombre);
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_cliente_changes
AFTER INSERT OR UPDATE OR DELETE ON Cliente
FOR EACH ROW EXECUTE FUNCTION log_cliente_changes();

CREATE OR REPLACE FUNCTION log_informemantenimiento_changes() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('InformeMantenimiento', 'INSERT', 'Inserted ID: ' || NEW.InformeID || ', Type: ' || NEW.TipoDeMantenimiento);
    ELSIF TG_OP = 'UPDATE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('InformeMantenimiento', 'UPDATE', 'Updated ID: ' || NEW.InformeID || ', Type: ' || NEW.TipoDeMantenimiento);
    ELSIF TG_OP = 'DELETE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('InformeMantenimiento', 'DELETE', 'Deleted ID: ' || OLD.InformeID || ', Type: ' || OLD.TipoDeMantenimiento);
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_informemantenimiento_changes
AFTER INSERT OR UPDATE OR DELETE ON InformeMantenimiento
FOR EACH ROW EXECUTE FUNCTION log_informemantenimiento_changes();

CREATE OR REPLACE FUNCTION log_ordendetrabajo_changes() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('OrdenDeTrabajo', 'INSERT', 'Inserted ID: ' || NEW.OrdenID || ', Priority: ' || NEW.OrdenPrioridad);
    ELSIF TG_OP = 'UPDATE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('OrdenDeTrabajo', 'UPDATE', 'Updated ID: ' || NEW.OrdenID || ', Priority: ' || NEW.OrdenPrioridad);
    ELSIF TG_OP = 'DELETE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('OrdenDeTrabajo', 'DELETE', 'Deleted ID: ' || OLD.OrdenID || ', Priority: ' || OLD.OrdenPrioridad);
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_ordendetrabajo_changes
AFTER INSERT OR UPDATE OR DELETE ON OrdenDeTrabajo
FOR EACH ROW EXECUTE FUNCTION log_ordendetrabajo_changes();

CREATE OR REPLACE FUNCTION log_tareamantenimiento_changes() RETURNS TRIGGER AS $$
BEGIN
    IF TG_OP = 'INSERT' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('TareaMantenimiento', 'INSERT', 'Inserted ID: ' || NEW.TareaID || ', Priority: ' || NEW.Prioridad);
    ELSIF TG_OP = 'UPDATE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('TareaMantenimiento', 'UPDATE', 'Updated ID: ' || NEW.TareaID || ', Priority: ' || NEW.Prioridad);
    ELSIF TG_OP = 'DELETE' THEN
        INSERT INTO Reportes (NombreTabla, TipoOperacion, OperacionDetalles)
        VALUES ('TareaMantenimiento', 'DELETE', 'Deleted ID: ' || OLD.TareaID || ', Priority: ' || OLD.Prioridad);
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_tareamantenimiento_changes
AFTER INSERT OR UPDATE OR DELETE ON TareaMantenimiento
FOR EACH ROW EXECUTE FUNCTION log_tareamantenimiento_changes();

INSERT INTO Proveedor (ProveedorID, ProveedorNombre, ProveedorTelefono, ProveedorDireccion) VALUES 
(1, 'Maquinaria S.A.', '123-456-7890', 'Calle 123, Ciudad Industrial'),
(2, 'Partes y Piezas', '098-765-4321', 'Avenida 45, Zona Comercial'),
(3, 'Equipos Modernos', '555-123-4567', 'Boulevard Central 789, Área Empresarial'),
(4, 'Proveedora Tech', '444-555-6666', 'Carretera 22, Parque Industrial'),
(5, 'Soluciones Mecánicas', '333-444-5555', 'Plaza Industrial 56, Distrito Tecnológico'),
(6, 'Componentes Industriales', '222-333-4444', 'Pasaje 78, Sector Industrial'),
(7, 'Técnica Global', '111-222-3333', 'Ruta 66, Polo Logístico'),
(8, 'Innovación y Maquinaria', '777-888-9999', 'Callejón 10, Centro Comercial'),
(9, 'Distribuciones Mecánicas', '666-777-8888', 'Parque 45, Área Industrial'),
(10, 'Servicios Industriales', '888-999-0000', 'Alameda 123, Ciudad Empresarial');
INSERT INTO Cliente (ClienteID, ClienteNombre, ClienteApellidos, ClienteTelefono, ClienteDirrecion) VALUES
(1, 'Juan', 'Pérez García', '555-1234', 'Av. Reforma 123, Ciudad de México'),
(2, 'Ana', 'Martínez López', '555-5678', 'Calle 5 de Mayo 456, Puebla'),
(3, 'Carlos', 'Sánchez Rodríguez', '555-8765', 'Boulevard de los Héroes 789, Monterrey'),
(4, 'María', 'González Fernández', '555-4321', 'Avenida Insurgentes 321, Guadalajara'),
(5, 'Luis', 'Hernández Ramírez', '555-6789', 'Calle Juárez 654, Querétaro'),
(6, 'Laura', 'Ramírez Martínez', '555-9876', 'Calle Madero 987, San Luis Potosí'),
(7, 'Pedro', 'García Torres', '555-3456', 'Avenida Universidad 345, León'),
(8, 'Carmen', 'López Jiménez', '555-6543', 'Calle Hidalgo 543, Mérida'),
(9, 'Jorge', 'Martínez Sánchez', '555-2345', 'Boulevard Belisario Domínguez 234, Tijuana'),
(10, 'Rosa', 'Flores Gutiérrez', '555-5432', 'Avenida de la Paz 432, Veracruz');
INSERT INTO Equipo (EquipoID, ProveedorID, Especificaciones, FechaServicio, MantenimientoPrevio) VALUES
(1, 1, 'Excavadora - Modelo X', '2023-01-15', 'Cambio de aceite'),
(2, 2, 'Bulldozer - Modelo B', '2023-02-20', 'Reemplazo de filtro'),
(3, 3, 'Grua - Modelo G', '2023-03-10', 'Inspección general'),
(4, 4, 'Compactador - Modelo C', '2023-04-25', 'Reparación de sistema hidráulico'),
(5, 5, 'Retroexcavadora - Modelo R', '2023-05-05', 'Ajuste de frenos'),
(6, 6, 'Minicargadora - Modelo M', '2023-06-15', 'Cambio de neumáticos'),
(7, 7, 'Pavimentadora - Modelo P', '2023-07-20', 'Revisión de motor'),
(8, 8, 'Cargador frontal - Modelo F', '2023-08-30', 'Reemplazo de batería'),
(9, 9, 'Excavadora - Modelo E', '2023-09-25', 'Lubricación de piezas móviles'),
(10, 10, 'Bulldozer - Modelo D', '2023-10-10', 'Ajuste de cadenas');
INSERT INTO OrdenDeTrabajo (OrdenID, ClienteID, EquipoID, OrdenFecha, OrdenPrioridad, DescripcionTrabajo) VALUES
(1, 1, 1, '2024-01-10', 'Alta', 'Revisión completa'),
(2, 2, 2, '2024-01-15', 'Media', 'Cambio de aceite'),
(3, 3, 3, '2024-01-20', 'Baja', 'Reemplazo de filtro de aire'),
(4, 4, 4, '2024-01-25', 'Alta', 'Reparación de sistema hidráulico'),
(5, 5, 5, '2024-02-01', 'Media', 'Ajuste de frenos'),
(6, 6, 6, '2024-02-05', 'Alta', 'Cambio de neumáticos'),
(7, 7, 7, '2024-02-10', 'Baja', 'Revisión de motor'),
(8, 8, 8, '2024-02-15', 'Media', 'Reemplazo de batería'),
(9, 9, 9, '2024-02-20', 'Alta', 'Lubricación de piezas móviles'),
(10, 10, 10, '2024-02-25', 'Baja', 'Inspección general');
INSERT INTO InformeMantenimiento (InformeID, OrdenID, Fecha, TipoDeMantenimiento, TiempoEmpleado, MaterialesUtilizados) VALUES
(1, 1, '2024-01-11', 'Revisión completa y ajuste de todos los sistemas', '3 horas', 'Aceite, Filtros, Lubricante'),
(2, 2, '2024-01-16', 'Cambio de aceite y verificación de niveles', '1 hora', 'Aceite'),
(3, 3, '2024-01-21', 'Reemplazo de filtro de aire y limpieza del sistema', '1.5 horas', 'Filtro de aire, Limpiador'),
(4, 4, '2024-01-26', 'Reparación del sistema hidráulico', '4 horas', 'Sellos, Aceite hidráulico, Herramientas especializadas'),
(5, 5, '2024-02-02', 'Ajuste de frenos y prueba de frenado', '2 horas', 'Pastillas de freno, Herramientas'),
(6, 6, '2024-02-06', 'Cambio de neumáticos y alineación', '2.5 horas', 'Neumáticos, Herramientas de alineación'),
(7, 7, '2024-02-11', 'Revisión y ajuste del motor', '3 horas', 'Aceite, Filtros, Herramientas'),
(8, 8, '2024-02-16', 'Reemplazo de batería y verificación del sistema eléctrico', '1 hora', 'Batería, Herramientas eléctricas'),
(9, 9, '2024-02-21', 'Lubricación de todas las piezas móviles y ajuste general', '2 horas', 'Lubricante, Herramientas'),
(10, 10, '2024-02-26', 'Inspección general y mantenimiento preventivo', '2.5 horas', 'Aceite, Filtros, Lubricante, Herramientas');
INSERT INTO TareaMantenimiento (TareaID, OrdenID, FechaLimite, Prioridad) VALUES
(1, 1, '2024-01-12', 'Alta'),
(2, 2, '2024-01-17', 'Media'),
(3, 3, '2024-01-22', 'Baja'),
(4, 4, '2024-01-27', 'Alta'),
(5, 5, '2024-02-03', 'Media'),
(6, 6, '2024-02-07', 'Alta'),
(7, 7, '2024-02-12', 'Baja'),
(8, 8, '2024-02-17', 'Media'),
(9, 9, '2024-02-22', 'Alta'),
(10, 10, '2024-02-27', 'Baja');